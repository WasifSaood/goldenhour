//
//  HomeViewExtension.swift
//  GoldenHour
//
//  Created by Wasif Saood on 30/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit
import MapKit

extension HomeViewController : CLLocationManagerDelegate {
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }

   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        let span = MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)        
        self.mapView.setRegion(region, animated: true)
        
        if self.mapView.annotations.count != 0 {
            annotation = self.mapView.annotations[0]
            self.mapView.removeAnnotation(annotation)
        }
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = location.coordinate
        pointAnnotation.title = ""
        mapView.addAnnotation(pointAnnotation)
        
        pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)

        viewModel.claculateSunTime(onDate: Date(), atLatitude: pointAnnotation.coordinate.latitude, andLongitude: location.coordinate.longitude,completion: { (response) -> Void in
            
        })
        let pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: nil)
        DispatchQueue.main.async {
            self.mapView.centerCoordinate = pointAnnotation.coordinate
            self.mapView.addAnnotation(pinAnnotationView.annotation!)
            let span = MKCoordinateSpan(latitudeDelta: 1.5, longitudeDelta: 1.5)
            let region = MKCoordinateRegion(center: pointAnnotation.coordinate, span: span)
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(String(describing: error))")
        spinner?.dismissLoader()
        self.view.isUserInteractionEnabled = true
    }
    
}

// MARK: - UISearchBar Delegate
extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        let spinner = showLoader(view: self.view)
        self.view.isUserInteractionEnabled = false
        if mapView.annotations.count != 0 {
            annotation = mapView.annotations[0]
            mapView.removeAnnotation(annotation)
        }
        
        localSearchRequest = MKLocalSearch.Request()
        localSearchRequest.naturalLanguageQuery = searchBar.text
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.start { [weak self] (localSearchResponse, error) -> Void in
        spinner.dismissLoader()
        self?.view.isUserInteractionEnabled = true
        if localSearchResponse == nil {
            self?.showAlertMessage(titleStr: "Error", messageStr: "Place not found")
            return
        }
        self?.searchController.isActive = false
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.title = searchBar.text
        pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
        self?.dateLabel.text = currentDate(byDate: Date())
        self?.viewModel.claculateSunTime(onDate: Date(), atLatitude: localSearchResponse!.boundingRegion.center.latitude, andLongitude: localSearchResponse!.boundingRegion.center.longitude,completion: { (response) -> Void in
            
        })
        let pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: nil)
             DispatchQueue.main.async {
                self?.mapView.centerCoordinate = pointAnnotation.coordinate
                self?.mapView.addAnnotation(pinAnnotationView.annotation!)
                let span = MKCoordinateSpan(latitudeDelta: 1.5, longitudeDelta: 1.5)
                let region = MKCoordinateRegion(center: pointAnnotation.coordinate, span: span)
                self?.mapView.setRegion(region, animated: true)
            }
        
      }
    }
}
