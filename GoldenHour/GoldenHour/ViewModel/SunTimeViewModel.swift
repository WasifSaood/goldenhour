//
//  SunTimeViewModel.swift
//  GoldenHour
//
//  Created by Wasif Saood on 30/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation
import MapKit

class  SunTimeViewModel {
    
    //Mark:Variables
    
    var currentDate:Date = Date()
    var currentlat:Double = 0.0
    var currentLong:Double = 0.0
    
    // MARK: Events
    var reloadSunTime: (_ sunsetTime : String, _ sunRiseTime: String, _ moonRise: String, _ moonSet: String)->() = { _,_,_,_  in }
    
    init() {
    }
    
    func claculateSunTime(onDate: Date, atLatitude latitude: Double, andLongitude longitude: Double, completion: @escaping (Bool) -> ())
    {
        currentDate = onDate
        currentlat = latitude
        currentLong = longitude
        let sunrise = SunTimeCaluction.sunPhaseTime(forPhase: .sunrise, withZenithType: .official, onDay: onDate, atLatitude: latitude, andLongitude: longitude)
        let sunset = SunTimeCaluction.sunPhaseTime(forPhase: .sunset, withZenithType: .official, onDay: onDate, atLatitude: latitude, andLongitude: longitude)
        let moon: MoonCalculation! = MoonCalculation()
        let time = moon.getMoonTimes(date: onDate, lat: latitude, lng: longitude)
        
         let location = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                self.reloadSunTime("_" , "_","_","_" )
                completion(false)
                return
            }else if let _ = placemarks?.first?.locality {
                let sunrise = UTCToLocal(date: sunrise ?? Date(), fromFormat: "yyy-MM-dd H:mm:ss", toFormat: "hh:mm a", timeZone: placemarks?.last?.timeZone?.identifier ?? "Asia/Kolkata") as Any
                let sunset = UTCToLocal(date: sunset ?? Date(), fromFormat: "yyy-MM-dd H:mm:ss", toFormat: "hh:mm a", timeZone: placemarks?.last?.timeZone?.identifier ?? "Asia/Kolkata") as Any
                let moonRise = UTCToLocal(date: time["rise"] as? Date ?? Date() , fromFormat: "yyy-MM-dd H:mm:ss", toFormat: "hh:mm a", timeZone: placemarks?.last?.timeZone?.identifier ?? "Asia/Kolkata") as Any
                let moonset = UTCToLocal(date: time["set"] as? Date ?? Date() , fromFormat: "yyy-MM-dd H:mm:ss", toFormat: "hh:mm a", timeZone: placemarks?.last?.timeZone?.identifier ?? "Asia/Kolkata") as Any
                self.reloadSunTime(sunset as! String , sunrise as! String, moonRise as! String , moonset as! String)
                completion(true)
            }
            
        })
    }
}
