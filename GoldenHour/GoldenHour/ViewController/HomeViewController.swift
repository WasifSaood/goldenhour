 
//
//  ViewController.swift
//  GoldenHour
//
//  Created by Wasif Saood on 27/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import UIKit
import MapKit

class HomeViewController: UIViewController  {
    
    @IBOutlet weak var moonSetLabel: UILabel!
    @IBOutlet weak var moonRiseLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sunSetLabel: UILabel!
    @IBOutlet weak var sunRiseLabel: UILabel!
    
    internal let searchController = UISearchController(searchResultsController: nil)
    var spinner:UIActivityIndicatorView?
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    internal var localSearchRequest: MKLocalSearch.Request!
    internal var localSearch: MKLocalSearch!
    fileprivate var localSearchResponse: MKLocalSearch.Response!
    internal var annotation: MKAnnotation!
    internal var viewModel = SunTimeViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestWhenInUseAuthorization()
        determineMyCurrentLocation()
        mapView.mapType = .standard
        self.dateLabel.text = currentDate(byDate: Date())
        setupSearch()
        spinner = showLoader(view: self.view)
        observeEvents()
        
    }
    
    //MARK: Setup the Search Controller
    private func setupSearch () {
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
    }
    
    
    //MARK: Function to observe event call backs from the viewmodel.
    private func observeEvents() {
        self.view.isUserInteractionEnabled = false
        viewModel.reloadSunTime = { [weak self] (sunset: String?, sunsrise: String?, moonRise: String?, moonSet:String?)  in
            DispatchQueue.main.async {
                self?.view.isUserInteractionEnabled = true
                self?.spinner?.dismissLoader()
                self?.sunRiseLabel.text = sunsrise
                self?.sunSetLabel.text = sunset
                self?.moonRiseLabel.text = moonRise
                self?.moonSetLabel.text = moonSet
            }
        }
    }
    
    private func determineMyCurrentLocation() {
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied {
                DispatchQueue.main.async {
                    self.spinner?.dismissLoader()
                    self.showAlertMessage(titleStr: "Error", messageStr: "PLease enable location services from settings of GoldenHour Application")
                }
            }
            
            locationManager.desiredAccuracy = 1.0
            locationManager.delegate = self
            locationManager.distanceFilter = 5000
            locationManager.startUpdatingLocation()
            
        } else {
            self.showAlertMessage(titleStr: "Error", messageStr: "PLease turn on location services or GPS")
            return
        }
    }
    
    @IBAction func previousDayButtonAction(_ sender: Any) {
        let spinner = showLoader(view: self.view)
        let newDate = Calendar.current.date(byAdding: .day, value: -1, to: viewModel.currentDate)!
        self.dateLabel.text = currentDate(byDate: newDate)
        viewModel.claculateSunTime(onDate: newDate, atLatitude: viewModel.currentlat, andLongitude: viewModel.currentLong,completion: { (response) -> Void in
            spinner.dismissLoader()
        })
    }
    @IBAction func nextDayButtonAction(_ sender: Any) {
        let spinner = showLoader(view: self.view)
        let newDate = Calendar.current.date(byAdding: .day, value: 1, to: viewModel.currentDate)!
        self.dateLabel.text = currentDate(byDate: newDate)
        viewModel.claculateSunTime(onDate: newDate, atLatitude: viewModel.currentlat, andLongitude: viewModel.currentLong, completion: { (response) -> Void in
            spinner.dismissLoader()
        })
    }
    @IBAction func resetButtonAction(_ sender: Any) {
        let spinner = showLoader(view: self.view)
        self.dateLabel.text = currentDate(byDate: Date())
        viewModel.claculateSunTime(onDate:  Date(), atLatitude: viewModel.currentlat, andLongitude: viewModel.currentLong, completion: { (response) -> Void in
            spinner.dismissLoader()
        })
    }
}
