//
//  SunTimeCaluction.swift
//  GoldenHour
//
//  Created by Wasif Saood on 29/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

final public class SunTimeCaluction {
    
    //MARK: Types
    public enum SunPhase {
        case sunrise
        case sunset
    }
    
    public enum Zenith: Double {
        case official = 90.83
        case civil = 96.0
        case nautical = 102.0
        case astronomical = 108.0
    }
    
    //MARK: Public functions
    public static func sunPhaseTime(forPhase phase: SunPhase, withZenithType zenithType: Zenith = .official, onDay date: Date, atLatitude latitude: Double, andLongitude longitude: Double) -> Date? {
        
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        
        let dateComponents = calendar.dateComponents([ .timeZone, .day, .month, .year ], from: date)
        
        let day = dateComponents.day!
        let month = dateComponents.month!
        let year = dateComponents.year!
        
        //Calculate day of the year
        let n1 = floor(275 * Double(month) / 9)
        let n2 = floor(Double(month + 9) / 12)
        let n3 = 1 + floor((Double(year) - 4 * floor(Double(year) / 4) + 2) / 3)
        let dayOfYear = n1 - (n2 * n3) + Double(day) - 30
        
        //Convert longitude to hour value
        let lngHour = longitude / 15
        
        let t: Double
        switch phase {
        case .sunrise:
            t = dayOfYear + ((6 - lngHour) / 24)
        case .sunset:
            t = dayOfYear + ((18 - lngHour) / 24)
        }
        
        //Sun's mean anomaly
        let meanAnomaly = (0.9856 * t) - 3.289
        
        //Sun's true longitude
        let trueLongitude = fixRange(meanAnomaly + (1.916 * sin(degrees: meanAnomaly)) + (0.020 * sin(degrees: 2 * meanAnomaly)) + 282.634, max: 360)
        
        //Sun's right ascension
        var rightAscension = fixRange(atan(degrees: 0.91764 * tan(degrees: trueLongitude)), max: 360)
        let lQuadrant  = (floor(trueLongitude / 90)) * 90
        let raQquadrant = (floor(rightAscension / 90)) * 90
        rightAscension += (lQuadrant - raQquadrant) //needs to be in same quandrant as l
        rightAscension /= 15 //convert to hours
        
        //Sun's declination
        let sinDec = 0.39782 * sin(degrees: trueLongitude)
        let cosDec = cos(degrees: asin(degrees: sinDec))
        
        //Sun's local hour angle
        let cosH = (cos(degrees: Double(zenithType.rawValue)) - (sinDec * sin(degrees: latitude))) / (cosDec * cos(degrees: latitude))
        
        if (cosH >  1) {
            return nil
        }
        else if (cosH < -1) {
            return nil
        }
        
        //Calculate hour
        let hour: Double
        switch phase {
        case .sunrise:
            hour = (360 - acos(degrees: cosH)) / 15
        case .sunset:
            hour = acos(degrees: cosH) / 15
        }
        
        //Local time of rising/setting
        let localTime = hour + rightAscension - (0.06571 * t) - 6.622
        
        let localTimeHourUTC = fixRange(localTime - lngHour, max: 24)
        
        var resultDateComponentsUTC = DateComponents()
        resultDateComponentsUTC.timeZone = calendar.timeZone
        resultDateComponentsUTC.calendar = calendar
        resultDateComponentsUTC.day = dateComponents.day
        resultDateComponentsUTC.month = dateComponents.month
        resultDateComponentsUTC.year = dateComponents.year
        resultDateComponentsUTC.hour = Int(localTimeHourUTC)
        resultDateComponentsUTC.minute = Int((localTimeHourUTC - floor(localTimeHourUTC)) * 60)
        
        let resultDate = calendar.date(from: resultDateComponentsUTC)
        
        return resultDate
        
    }
    
}

//MARK:- Convenience functions
fileprivate extension SunTimeCaluction {
    static func fixRange(_ x: Double, max: Double) -> Double {
        return x - max * floor(x * (1 / max))
    }
    
}
