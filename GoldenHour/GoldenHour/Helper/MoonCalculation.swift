//
//  MoonCalculation.swift
//  GoldenHour
//
//  Created by Wasif Saood on 30/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

public class MoonCalculation
{
    // Simple conventions
    internal let PI: Double = Double.pi
    internal let rad: Double = Double.pi / 180.0
    
    // Date/time conversions (Julian time)
    internal let dayMs: Double = 1000.0 * 60.0 * 60.0 * 24.0
    internal let J1970: Double = 2440588.0
    internal let J2000: Double = 2451545.0
    
    // Obliquity of the Earth
    internal let e: Double = (Double.pi / 180.0) * 23.4397
 
    private func toJulian(date: Date) -> Double
    {
        return Double(date.millisecondsSince1970) / dayMs - 0.5 + J1970
    }

    private func toDays(date: Date) -> Double
    {
        return toJulian(date: date) - J2000
    }
    
    private func rightAscension(l: Double, b: Double) -> Double
    {
        return atan2(sin(l) * cos(e) - tan(b) * sin(e), cos(l))
    }
    
    private func declination(l: Double, b: Double) -> Double
    {
        return asin(sin(b) * cos(e) + cos(b) * sin(e) * sin(l))
    }
    
    
    private func azimuth(H: Double, phi: Double, dec: Double) -> Double
    {
        return atan2(sin(H), cos(H) * sin(phi) - tan(dec) * cos(phi))
    }
    
    private func altitude(H: Double, phi: Double, dec: Double) -> Double
    {
        return asin(sin(phi) * sin(dec) + cos(phi) * cos(dec) * cos(H));
    }
    
    private func siderealTime(d: Double, lw: Double) -> Double
    {
        return rad * (280.16 + 360.9856235 * d) - lw
    }
    
    private func astroRefraction(h: Double) -> Double
    {
        var h: Double = h
      
        if (h < 0.0)
        {
            h = 0.0
        }
        return 0.0002967 / tan(h + 0.00312536 / (h + 0.08901179))
    }
   
  
    private func moonCoords(d: Double) -> Dictionary<String, Double>
    {
        // Ecliptic longitude
        let L: Double = rad * (218.316 + 13.176396 * d),
        // Mean anomaly
        M: Double = rad * (134.963 + 13.064993 * d),
        // Mean distance
        F: Double = rad * (93.272 + 13.229350 * d),
        // Longitude
        l: Double = L + rad * 6.289 * sin(M),
        // Latitude
        b: Double = rad * 5.128 * sin(F),
        // Distance to the moon in km
        dt: Double = 385001.0 - 20905.0 * cos(M)
        
        return [
            "ra": rightAscension(l: l, b: b),
            "dec": declination(l: l, b: b),
            "dist": dt
        ]
    }
   
    private func hoursLater(date: Date, h: Double) -> Date
    {
        return Date(milliseconds: Int(Double(date.millisecondsSince1970) + h * dayMs  / 24.0))
    }
    public init()
    {
        
    }
    
    public func getMoonPosition(date: Date, lat: Double, lng: Double) -> Dictionary<String, Double>
    {
        var lw: Double = rad * -lng,
        phi: Double = rad * lat,
        d: Double = toDays(date: date),
        
        c: Dictionary<String, Double> = moonCoords(d: d),
        H: Double = siderealTime(d: d, lw: lw) - c["ra"]!,
        alt: Double = altitude(H: H, phi: phi, dec: c["dec"]!),
        // Formula 14.1 of "Astronomical Algorithms" 2nd edition by Jean Meeus (Willmann-Bell,
        // Richmond) 1998.
        pa: Double = atan2(sin(H), tan(phi) * cos(c["dec"]!) - sin(c["dec"]!) * cos(H)),
        // Altitude correction for refraction
        h: Double = alt + astroRefraction(h: alt)
        
        return [
            "azimuth": azimuth(H: H, phi: phi, dec: c["dec"]!),
            "altitude": h,
            "distance": c["dist"]!,
            "parallacticAngle": pa
        ]
    }
    
    public func getMoonTimes(date: Date, lat: Double, lng: Double) -> Dictionary<String, Date?>
    {
        var t: Date =  Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date)!,
            hc: Double = 0.133 * rad,
            h0: Double = getMoonPosition(date: t, lat: lat, lng: lng)["altitude"]! - hc,
            h1: Double,
            h2: Double,
            rise: Double? = nil,
            set: Double? = nil,
            a: Double,
            b: Double,
            xe: Double,
            ye: Double,
            d: Double,
            roots: Int,
            x1: Double? = nil,
            x2: Double? = nil,
            dx: Double
        
        // Go in 2-hour chunks, each time seeing if a 3-point quadratic curve crosses zero
        // (which means rise or set)
        for i in stride(from: 1, to: 24, by: 2)
        {
            h1 = getMoonPosition(date: hoursLater(date: t,
                                                  h: Double(i)), lat: lat, lng: lng)["altitude"]! - hc
            h2 = getMoonPosition(date: hoursLater(date: t,
                                                  h: Double(i + 1)), lat: lat, lng: lng)["altitude"]! - hc
            a = (h0 + h2) / 2.0 - h1
            b = (h2 - h0) / 2.0
            xe = -b / (2.0 * a)
            ye = (a * xe + b) * xe + h1
            d = b * b - 4.0 * a * h1
            roots = 0
            
            if d >= 0.0
            {
                dx = Double(sqrt(d)) / Double(abs(a) * 2.0)
                x1 = xe - dx
                x2 = xe + dx
                if abs(x1!) <= 1.0
                {
                    roots = roots + 1
                }
                if abs(x2!) <= 1.0
                {
                    roots = roots + 1
                }
                if x1! < -1.0
                {
                    x1 = x2
                }
            }
            
            if roots == 1
            {
                if h0 < 0.0
                {
                    rise = Double(i) + x1!
                }
                else
                {
                    set = Double(i) + x1!
                }
            }
            else if roots == 2
            {
                rise = Double(i) + (ye < 0.0 ? x2! : x1!)
                set = Double(i) + (ye < 0.0 ? x1! : x2!)
            }
            
            if (rise != nil && set != nil)
            {
                break
            }
            
            h0 = h2
        }
        
        var result: Dictionary<String, Date?> = ["rise": nil, "set": nil]
        
        if (rise != nil)
        {
            result["rise"] = hoursLater(date: t, h: rise!)
        }
        if (set != nil)
        {
            result["set"] = hoursLater(date: t, h: set!)
        }
                
        return result
    }
}

fileprivate extension Date
{
    // Extended functionality to get milliseconds from 1970 to present time
    var millisecondsSince1970:Int
    {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    // Obtaining the time interval since 1970 for a given amount of milliseconds
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
