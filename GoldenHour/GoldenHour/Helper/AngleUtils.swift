//
//  AngleUtils.swift
//  GoldenHour
//
//  Created by Wasif Saood on 29/07/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//
import Foundation

func sin(degrees: Double) -> Double {
    return __sinpi(degrees/180.0)
}

func cos(degrees: Double) -> Double {
    return __cospi(degrees/180.0)
}

func tan(degrees: Double) -> Double {
    return __tanpi(degrees/180.0)
}

func atan(degrees: Double) -> Double {
    return atan(degrees) * (180.0 / Double.pi)
}

func acos(degrees: Double) -> Double {
    return acos(degrees) * (180.0 / Double.pi)
}

func asin(degrees: Double) -> Double {
    return asin(degrees) * (180.0 / Double.pi)
}
